{*******************************************************}
{                                                       }
{       VirtualListBox                                  }
{                                                       }
{       Copyright (C) 2018 Viktor Akselrod              }
{                                                       }
{*******************************************************}
unit VirtualListBox.Registration;

interface

uses
  System.Classes,
  FMX.VirtualListBox;

procedure Register();

implementation

procedure Register();
begin
  RegisterComponents('Additional', [TVirtualListBox]);
end;

end.
