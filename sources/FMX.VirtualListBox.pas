{*******************************************************}
{                                                       }
{       VirtualListBox                                  }
{                                                       }
{       Copyright (C) 2018 Viktor Akselrod              }
{                                                       }
{*******************************************************}
unit FMX.VirtualListBox;

interface

uses
  System.SysUtils,
  System.Classes,
  System.UITypes,
  System.Math,
  System.Generics.Collections,
  FMX.Types,
  FMX.Controls,
  FMX.Layouts,
  FMX.StdCtrls,
  FMX.Platform,
  FMX.Ani,
  FMX.InertialMovement;

type
  TViewClass = class of TControl;

  TItemSettings = record
    Height : Int32;
    View   : TViewClass;
  end;

  TGetItemsCountEvent     = function (Sender: TObject): Int32 of object;
  TGetItemSettingsEvent   = procedure (Sender: TObject; const AIndex: Int32; out ASettings: TItemSettings) of object;
  TGetItemDataEvent       = procedure (Sender: TObject; const AIndex: Int32; const AView: TControl) of object;
  TItemViewCustomizeEvent = procedure (Sender: TObject; const AView: TControl) of object;

  TVirtualListBox = class(TLayout)
  strict private
  type
    TViews = TList<TControl>;
  const
  {$IFDEF IOS}
    DefaultScrollBarWidth = 7;
  {$ELSE}
  {$IFDEF MACOS}
    DefaultScrollBarWidth = 7;
  {$ENDIF}
  {$ENDIF}
  {$IFDEF MSWINDOWS}
    DefaultScrollBarWidth = 16;
  {$ENDIF}
  {$IFDEF ANDROID}
    DefaultScrollBarWidth = 7;
  {$ENDIF}
  var
    FFullHeight           : Single;
    FFullWidth            : Single;
    FScrollBar            : TScrollBar;
    FTouchScrollScreen    : TControl;
    FTouchScroll          : TAniCalculations;
    FItemsSettings        : TArray<TItemSettings>;
    FCurrentItems         : TViews;
    FViewsCache           : TObjectDictionary<TViewClass,TViews>;
    FOnGetItemsCount      : TGetItemsCountEvent;
    FOnGetItemSettings    : TGetItemSettingsEvent;
    FOnGetItemData        : TGetItemDataEvent;
    FOnItemViewCustomize  : TItemViewCustomizeEvent;
    procedure ScrollBarChange(Sender: TObject);
    procedure UpdateScrollBar(const AResetPosition: Boolean);
    procedure UpdateCurrentPage(const APosition: Single);
    procedure TouchScrollScreenMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
    // TouchScroll.
    procedure TouchScrollStart(Sender: TObject);
    procedure TouchScrollStop(Sender: TObject);
    procedure TouchScrollChanged(Sender: TObject);
    // Views storage.
    procedure ReleaseView(const AView: TControl);
    function GetView(const AClass: TViewClass): TControl;
    function FindViewInCache(const AClass: TViewClass): TControl;
  protected
    procedure Resize(); override;
    procedure MouseWheel(Shift: TShiftState; WheelDelta: Integer; var Handled: Boolean); override;
    procedure CMGesture(var EventInfo: TGestureEventInfo); override;
    procedure DoMouseLeave(); override;
    procedure Paint(); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;

    procedure Update(const AResetScroll: Boolean = False);
  published
    property Align;
    property Anchors;
    property ClipChildren;
    property ClipParent;
    property Cursor;
    property DragMode;
    property EnableDragHighlight;
    property Enabled;
    property Locked;
    property Height;
    property HitTest default False;
    property Padding;
    property Opacity;
    property Margins;
    property PopupMenu;
    property Position;
    property RotationAngle;
    property RotationCenter;
    property Scale;
    property ScrollBar: TScrollBar read FScrollBar;
    property Size;
    property TouchTargetExpansion;
    property Visible;
    property Width;
    property TabOrder;
    property TabStop;
    { Events }
    property OnPainting;
    property OnPaint;
    property OnResize;
    property OnResized;
    { Drag and Drop events }
    property OnDragEnter;
    property OnDragLeave;
    property OnDragOver;
    property OnDragDrop;
    property OnDragEnd;
    { Mouse events }
    property OnClick;
    property OnDblClick;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnMouseWheel;
    property OnMouseEnter;
    property OnMouseLeave;
    { Specific events }
    property OnGetItemsCount: TGetItemsCountEvent read FOnGetItemsCount write FOnGetItemsCount;
    property OnGetItemSettings: TGetItemSettingsEvent read FOnGetItemSettings write FOnGetItemSettings;
    property OnGetItemData: TGetItemDataEvent read FOnGetItemData write FOnGetItemData;
    property OnItemViewCustomize: TItemViewCustomizeEvent read FOnItemViewCustomize write FOnItemViewCustomize;
  end;

implementation

{ TVirtualListBox }

constructor TVirtualListBox.Create(AOwner: TComponent);
var
  LSystemInfoSrv : IFMXSystemInformationService;
begin
  inherited;
  FDisableAlign := True;

  FScrollBar := TScrollBar.Create(Self);
  FScrollBar.Stored := False;
  FScrollBar.Visible := False;
  FScrollBar.Orientation := TOrientation.Vertical;
  FScrollBar.Width := DefaultScrollBarWidth;
  FScrollBar.Locked := True;
  FScrollBar.Parent := Self;

  FViewsCache := TObjectDictionary<TViewClass,TViews>.Create([doOwnsValues]);
  FCurrentItems := TViews.Create();

  SupportsPlatformService(IFMXSystemInformationService, LSystemInfoSrv);
  if (LSystemInfoSrv = nil) or (not (TScrollingBehaviour.TouchTracking in LSystemInfoSrv.GetScrollingBehaviour)) then begin
    Exit;
  end;
  FTouchScroll := TAniCalculations.Create(Self);
  FTouchScroll.Animation := True;
  FTouchScroll.BoundsAnimation := False;
//  FTouchScroll.Interval := DefaultIntervalOfAni;
//  FTouchScroll.DecelerationRate := DecelerationRateNormal;
//  FTouchScroll.Elasticity := DefaultElasticity;
//  FTouchScroll.StorageTime := DefaultStorageTime;
  FTouchScroll.TouchTracking := [ttVertical];
  FTouchScroll.OnStart := TouchScrollStart;
  FTouchScroll.OnStop := TouchScrollStop;
  FTouchScroll.OnChanged := TouchScrollChanged;

  FTouchScrollScreen := TControl.Create(Self);
  FTouchScrollScreen.Parent := Self;
  FTouchScrollScreen.HitTest := True;
  FTouchScrollScreen.OnMouseDown := TouchScrollScreenMouseDown;

  Touch.DefaultInteractiveGestures := Touch.DefaultInteractiveGestures + [TInteractiveGesture.Pan];
  Touch.InteractiveGestures := Touch.InteractiveGestures + [TInteractiveGesture.Pan];
end;

destructor TVirtualListBox.Destroy();
begin
  FreeAndNil(FTouchScroll);
  FreeAndNil(FTouchScrollScreen);
  FreeAndNil(FScrollBar);
  FreeAndNil(FViewsCache);
  FreeAndNil(FCurrentItems);
  inherited;
end;

procedure TVirtualListBox.Update(const AResetScroll: Boolean);
var
  I : Int32;
begin
  // Get items count.
  if (Assigned(FOnGetItemsCount)) then begin
    SetLength(FItemsSettings, FOnGetItemsCount(Self));
  end else begin
    FItemsSettings := nil;
  end;
  // Get height and control class for all items.
  // Calculate total items height.
  FFullHeight := 0;
  if (Assigned(FOnGetItemSettings)) then begin
    for I := 0 to High(FItemsSettings) do begin
      FOnGetItemSettings(Self, I, FItemsSettings[I]);
      FFullHeight := FFullHeight + FItemsSettings[I].Height;
    end;
  end;
  // Recalculate vertical scroll bar.
  UpdateScrollBar(AResetScroll);
  // Show current items.
  UpdateCurrentPage(FScrollBar.Value);
end;

function TVirtualListBox.FindViewInCache(const AClass: TViewClass): TControl;
var
  LViews : TViews;
begin
  if (FViewsCache.TryGetValue(AClass, LViews)) and (LViews.Count > 0) then begin
    Result := LViews.First();
    LViews.Delete(0);
  end else begin
    Result := nil;
  end;
end;

function TVirtualListBox.GetView(const AClass: TViewClass): TControl;
begin
  Result := FindViewInCache(AClass);
  if (Result = nil) then begin
    Result := AClass.Create(nil);
    // Customize view if needed.
    if (Assigned(FOnItemViewCustomize)) then begin
      FOnItemViewCustomize(Self, Result);
    end;
    Result.Visible := False;
    Result.Parent := Self;
  end;
end;

procedure TVirtualListBox.ReleaseView(const AView: TControl);
var
  LViewClass : TViewClass;
  LViews     : TViews;
begin
  LViewClass := TViewClass(AView.ClassType);
  if (not FViewsCache.TryGetValue(LViewClass, LViews)) then begin
    LViews := TViews.Create();
    FViewsCache.Add(LViewClass, LViews);
  end;
  AView.Visible := False;
  LViews.Add(AView);
end;

procedure TVirtualListBox.Resize();
begin
  inherited;
  UpdateScrollBar(False);
  UpdateCurrentPage(FScrollBar.Value);
end;

procedure TVirtualListBox.ScrollBarChange(Sender: TObject);
begin
  UpdateCurrentPage(FScrollBar.Value);
end;

procedure TVirtualListBox.TouchScrollScreenMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  // Stop touch scrolling by tap.
  if (FTouchScroll <> nil) then begin
    FTouchScroll.MouseDown(X, Y);
    FTouchScroll.MouseUp(X, Y);
  end;
end;

procedure TVirtualListBox.UpdateCurrentPage(const APosition: Single);
var
  LView       : TControl;
  LItemTop    : Single;
  LFirstItem  : Int32;
  I           : Int32;
begin
  FDisablePaint := True;
  try
    // Hide old items.
    for I := 0 to FCurrentItems.Count - 1 do begin
      ReleaseView(FCurrentItems[I]);
    end;
    FCurrentItems.Clear();
    if (not Assigned(FOnGetItemData)) then begin
      Exit;
    end;
    // Get first visible item index and her top position.
    LFirstItem := - 1;
    LItemTop := 0;
    for I := 0 to High(FItemsSettings) do begin
      if (APosition >= LItemTop) and (APosition < LItemTop + FItemsSettings[I].Height) then begin
        LFirstItem := I;
        Break;
      end;
      LItemTop := LItemTop + FItemsSettings[I].Height;
    end;
    if (LFirstItem = -1) then begin
      Exit;
    end;
    // Get data for visible items and show it.
    LItemTop := LItemTop - APosition;
    for I := LFirstItem to High(FItemsSettings) do begin
      LView := GetView(FItemsSettings[I].View);
      FCurrentItems.Add(LView);
      FOnGetItemData(Self, I, LView);
      LView.SetBounds(0, LItemTop, FFullWidth, FItemsSettings[I].Height);
      LView.Visible := True;
      LItemTop := LItemTop + FItemsSettings[I].Height;
      if (LItemTop >= Height) then begin
        Break;
      end;
    end;
    // Bring to front transparent control for hook user tap for stop scrolling by tap.
    if (FTouchScrollScreen <> nil) and (FTouchScrollScreen.Visible) then begin
      FTouchScrollScreen.BringToFront();
    end;
  finally
    FDisablePaint := False;
  end;
end;

procedure TVirtualListBox.UpdateScrollBar(const AResetPosition: Boolean);
var
  LTargets : TArray<TAniCalculations.TTarget>;
begin
  FScrollBar.OnChange := nil;
  try
    FFullWidth := Width;
    FScrollBar.Max := Max(0, FFullHeight - Height);
    FScrollBar.SmallChange := Height / 5;
    FScrollBar.SetBounds(Width - FScrollBar.Width, 0, FScrollBar.Width, Height);
    FScrollBar.Visible := FScrollBar.Max > 0;
    if (AResetPosition) then begin
      FScrollBar.Value := 0;
    end;
    if (FTouchScroll <> nil) then begin
      SetLength(LTargets, 2);
      LTargets[0].TargetType := TAniCalculations.TTargetType.Min;
      LTargets[0].Point := TPointD.Create(0,0);
      LTargets[1].TargetType := TAniCalculations.TTargetType.Max;
      LTargets[1].Point := TPointD.Create(FFullWidth, FFullHeight - Height);
      FTouchScroll.SetTargets(LTargets);
      FScrollBar.Opacity := 0;
      FScrollBar.HitTest := False;
      FTouchScrollScreen.SetBounds(0, 0, FFullWidth, Height);
    end else begin
      FScrollBar.Opacity := 1;
      FScrollBar.HitTest := True;
      if (FScrollBar.Visible) then begin
        FFullWidth := FFullWidth - FScrollBar.Width;
      end;
    end;
  finally
    FScrollBar.OnChange := ScrollBarChange;
  end;
end;

procedure TVirtualListBox.DoMouseLeave();
begin
  if (FTouchScroll <> nil) then begin
    FTouchScroll.MouseLeave();
  end;
end;

procedure TVirtualListBox.MouseWheel(Shift: TShiftState; WheelDelta: Integer; var Handled: Boolean);
begin
  inherited;
  if (not Handled) and (not (ssHorizontal in Shift)) then begin
    FScrollBar.Value := FScrollBar.Value - FScrollBar.SmallChange * (WheelDelta / 120);
  end;
end;

procedure TVirtualListBox.Paint();
begin
  inherited;
  if (csDesigning in ComponentState) and (not Locked) then begin
    DrawDesignBorder();
  end;
end;

procedure TVirtualListBox.TouchScrollChanged(Sender: TObject);
begin
  FScrollBar.Value := FTouchScroll.ViewportPosition.Y;
end;

procedure TVirtualListBox.TouchScrollStart(Sender: TObject);
begin
  FTouchScrollScreen.Visible := True;
  TAnimator.StopPropertyAnimation(FScrollBar, 'Opacity');
  FScrollBar.Opacity := 1;
  if (Scene <> nil) then begin
    Scene.ChangeScrollingState(Self, True);
  end;
end;

procedure TVirtualListBox.TouchScrollStop(Sender: TObject);
begin
  FTouchScrollScreen.Visible := False;
  TAnimator.AnimateFloat(FScrollBar, 'Opacity', 0, 0.2);
  FScrollBar.Value := FTouchScroll.ViewportPositionF.Y;
  if (Scene <> nil) then begin
    Scene.ChangeScrollingState(nil, False);
  end;
end;

procedure TVirtualListBox.CMGesture(var EventInfo: TGestureEventInfo);
begin
  if (EventInfo.GestureID = igiPan) and (FTouchScroll <> nil) then begin
    if (TInteractiveGestureFlag.gfBegin in EventInfo.Flags) then begin
      FTouchScroll.MouseDown(EventInfo.Location.X, EventInfo.Location.Y);
    end else if (TInteractiveGestureFlag.gfEnd in EventInfo.Flags) then begin
      FTouchScroll.MouseUp(EventInfo.Location.X, EventInfo.Location.Y);
    end else begin
      FTouchScroll.MouseMove(EventInfo.Location.X, EventInfo.Location.Y);
    end;
  end else begin
    inherited;
  end;
end;

end.
