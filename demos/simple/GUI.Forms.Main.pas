unit GUI.Forms.Main;

interface

uses
  System.SysUtils,
  System.Types,
  System.UITypes,
  System.Classes,
  System.Variants,
  FMX.Types,
  FMX.Controls,
  FMX.Forms,
  FMX.Graphics,
  FMX.Dialogs,
  FMX.Layouts,
  FMX.VirtualListBox,
  GUI.Frames.Style1;

type
  TViewMain = class(TForm)
    lbxMain: TVirtualListBox;
    function lbxMainGetItemsCount(Sender: TObject): Integer;
    procedure lbxMainGetItemData(Sender: TObject; const AIndex: Integer;
      const AView: TControl);
    procedure lbxMainGetItemSettings(Sender: TObject; const AIndex: Integer;
      out ASettings: TItemSettings);
    procedure FormCreate(Sender: TObject);
  end;

var
  ViewMain: TViewMain;

implementation

{$R *.fmx}

procedure TViewMain.FormCreate(Sender: TObject);
begin
  lbxMain.Update();
end;

procedure TViewMain.lbxMainGetItemData(Sender: TObject; const AIndex: Integer;
  const AView: TControl);
begin
  (AView as TViewStyle1).txtCaption.Text := 'Item ' + AIndex.ToString();
end;

function TViewMain.lbxMainGetItemsCount(Sender: TObject): Integer;
begin
  Result := 1000;
end;

procedure TViewMain.lbxMainGetItemSettings(Sender: TObject;
  const AIndex: Integer; out ASettings: TItemSettings);
begin
  ASettings.Height := 46;
  ASettings.View := TViewStyle1;
end;

end.
