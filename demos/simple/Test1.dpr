program Test1;

uses
  System.StartUpCopy,
  FMX.Forms,
  GUI.Forms.Main in 'GUI.Forms.Main.pas' {ViewMain},
  GUI.Frames.Style1 in 'GUI.Frames.Style1.pas' {ViewStyle1: TFrame};

{$R *.res}

begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  Application.Initialize;
  Application.CreateForm(TViewMain, ViewMain);
  Application.Run;
end.
