unit GUI.Frames.Style1;

interface

uses
  System.SysUtils,
  System.Types,
  System.UITypes,
  System.Classes,
  System.Variants,
  FMX.Types,
  FMX.Graphics,
  FMX.Controls,
  FMX.Forms,
  FMX.Dialogs,
  FMX.StdCtrls,
  FMX.Objects,
  FMX.Controls.Presentation;

type
  TViewStyle1 = class(TFrame)
    btnMore: TSpeedButton;
    lnBottom: TLine;
    txtCaption: TText;
  end;

implementation

{$R *.fmx}

end.
